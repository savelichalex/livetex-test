'use strict';

import { fromJS } from 'immutable';

export const compose = function() {
	const funcs = arguments;
	return function() {
		let args = arguments;
		for(let i = funcs.length; i-- > 0;) {
			args = [funcs[i].apply(this, args)];
		}
		return args[0];
	};
};

export const createChanger = func => (state, args) => func({state, ...args});

const LOCAL_STORAGE_KEY = 'livetex-test';

export const saveStateToLocalStorageMiddleware = state => {
	localStorage.setItem(LOCAL_STORAGE_KEY, JSON.stringify(state));
	return state;
};

export const subscribeToStateOutterChanges = cb => {
	window.addEventListener('storage', ({key, newValue}) => key === LOCAL_STORAGE_KEY && cb(fromJS(JSON.parse(newValue))))
};

export const getSavedState = () => localStorage.getItem(LOCAL_STORAGE_KEY);

export const initApp = ({initialState, onInit, onOutterChanges}) => {
	const oldState = getSavedState();
	subscribeToStateOutterChanges(onOutterChanges);
	if(oldState) {
		return onInit(fromJS(JSON.parse(oldState)));
	} else {
		return onInit(initialState);
	}
};