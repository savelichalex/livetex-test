'use strict';

/**
 * How create your own "redux" like application from scratch
 */

import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Map, List} from 'immutable'; // using immutable because doesn't want to have deal with Object.assign right now
import {
	compose,
	saveStateToLocalStorageMiddleware,
	initApp
} from './util';
import changer from './changers'; //if you like, call it reducers, or whatever you want, this is doesn't matter

let state = new Map({
	notes: new List(),
	filterBy: "",
	filteredNotes: new List(),
	tempNote: new Map({
		title: '',
		text: ''
	})
});

const saveStateToLocalVariable = newState => (state = newState) && newState;
const prepareProps = state => ({state, dispatch: dispatch(state)});
const changeAndRenderState = compose(renderState, prepareProps, saveStateToLocalStorageMiddleware);
function dispatch(state) {
    return (action, obj) => changeAndRenderState(changer(state, action, obj));
}

import { Notes } from './components/Notes';
import { CreateNoteForm } from './components/CreateNote';
import { Filter } from './components/Filter';

const App = ({state, dispatch}) =>
	<div style={{display: "flex", flexDirection: "column", alignItems: "stretch", width: "300px", margin: "0 auto"}}>
		<Filter dispatch={dispatch} value={state.get('filterBy')} />
		<CreateNoteForm dispatch={dispatch} {...state.get('tempNote').toJS()} />
		<Notes notes={state.get('filteredNotes')} dispatch={dispatch} />
	</div>;

function renderState(props) {
	ReactDOM.render(<App {...props} />, document.getElementById('root'));
}

initApp({
	initialState: state,
	onInit: state => compose(renderState, prepareProps)(state),
	onOutterChanges: compose(renderState, prepareProps)
});