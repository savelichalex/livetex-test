'use strict';

import React from 'react';
import { CHANGE_NOTE_TITLE, CHANGE_NOTE_TEXT, DELETE_NOTE } from '../changers'

const styles = {
	noteContainer: {
		display: "flex",
		flexDirection: "row",
		height: "20px"
	},
	noteTitle: {
		flex: 5
	},
	noteText: {
		flex: 5
	},
	deleteButton: {
		display: "flex",
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
		cursor: "pointer"
	}
};

export const NoteTitle = ({value, onPress}) =>
	<div style={styles.noteTitle}>
		<input value={value} onChange={e => onPress(e.target.value)}/>
	</div>;

export const NoteText = ({value, onPress}) =>
	<div style={styles.noteTitle}>
		<input value={value} onChange={e => onPress(e.target.value)}/>
	</div>;

const DeleteButton = ({onPress}) =>
	<div style={styles.deleteButton}>
		<span onClick={onPress}>x</span>
	</div>;

export const Note = ({id, title, text, dispatch}) =>
	<div style={styles.noteContainer}>
		<NoteTitle value={title} onPress={value => dispatch(CHANGE_NOTE_TITLE, {id, value})} />
		<NoteText value={title} onPress={value => dispatch(CHANGE_NOTE_TEXT, {id, value})} />
		<DeleteButton onPress={() => dispatch(DELETE_NOTE, {id})} />
	</div>;