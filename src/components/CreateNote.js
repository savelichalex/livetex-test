'use strict';

import React from 'react';
import { NoteTitle, NoteText } from './Note';
import { CHANGE_TEMP_NOTE_TITLE, CHANGE_TEMP_NOTE_TEXT, CREATE_NOTE } from '../changers';

const styles = {
	createNote: {
		display: "flex",
		flexDirection: "row"
	},
	createButton: {
		cursor: "pointer"
	}
};

const CreateButton = ({onPress}) =>
	<div style={styles.createButton}>
		<span onClick={onPress}>+</span>
	</div>;

export const CreateNoteForm = ({title, text, dispatch}) => {
	return (
		<div style={styles.createNote}>
			<NoteTitle value={title} onPress={value => dispatch(CHANGE_TEMP_NOTE_TITLE, {value})} />
			<NoteText value={text} onPress={value => dispatch(CHANGE_TEMP_NOTE_TEXT, {value})} />
			<CreateButton onPress={() => dispatch(CREATE_NOTE)} />
		</div>
	);
};