'use strict';

import React from 'react';
import { Note } from './Note';

const styles = {
	notes: {
		display: 'flex',
		flexDirection: 'column',
		alignItems: 'stretch'
	}
};

export const Notes = ({notes, dispatch}) =>
	<div style={styles.notes}>
		{notes.map((note, index) => <Note key={index} id={index} dispatch={dispatch} {...note.toJS()} />)}
	</div>;