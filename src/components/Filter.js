'use strict';

import React from 'react'; 
import { CHANGE_FILTER } from '../changers';

const styles = {
	filterContainer: {
		display: "flex",
		alignItems: "stretch"
	}
};

export const Filter = ({value, dispatch}) =>
	<div style={styles.filterContainer}>
		<input value={value} onChange={e => dispatch(CHANGE_FILTER, {value: e.target.value})}/>
	</div>;