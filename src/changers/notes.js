'use strict';

import { Map } from 'immutable';
import { createChanger, compose } from '../util';

const changeTitle = value => note => note.set('title', value);
const changeText = value => note => note.set('text', value);
const createNote = (title, text) => new Map({title, text});

const changeTempNoteTitle = ({state, value}) => state.update('tempNote', changeTitle(value));
const changeTempNoteText = ({state, value}) => state.update('tempNote', changeText(value));
const validateNewNote = ({state}) => {
	const {
		title,
		text
	} = state.get('tempNote').toJS();
	return {state, valid: (title && text), title, text};
};
const createNewNote = ({state, valid, title, text}) =>
	!valid ?
		state :
		state
			.update('notes', notes => notes.push(createNote(title, text)))
			.update('tempNote', note => changeTitle("")(changeText("")(note)));

const changeNoteTitle = ({state, id, value}) => state.updateIn(['notes', id], changeTitle(value));
const changeNoteText = ({state, id, value}) => state.updateIn(['notes', id], changeText(value));
const deleteNote = ({state, id}) => state.deleteIn(['notes', id]);
const changeFilter = ({state, value}) => state.set('filterBy', value);

/**
 * This also can be middleware,
 * but this decision more flexible and doesn't do unnecessary calculations
 */
const filterNotes = state => {
	const filterPredicate = state.get('filterBy');
	return state
		.set('filteredNotes',
			state
				.get('notes')
				.filter(note => note.get('title').indexOf(filterPredicate) !== -1)
		);
};

export const createNewNoteChanger = createChanger(compose(filterNotes, createNewNote, validateNewNote));
export const changeTempNoteTitleChanger = createChanger(changeTempNoteTitle);
export const changeTempNoteTextChanger = createChanger(changeTempNoteText);
export const changeNoteTitleChanger = createChanger(changeNoteTitle);
export const changeNoteTextChanger = createChanger(changeNoteText);
export const deleteNoteChanger = createChanger(compose(filterNotes, deleteNote));
export const changeFilterChanger = createChanger(compose(filterNotes, changeFilter));