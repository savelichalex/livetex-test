'use strict';

export const CHANGE_NOTE_TITLE = 'CHANGE_NOTE_TITLE';
export const CHANGE_NOTE_TEXT = 'CHANGE_NOTE_TEXT';
export const DELETE_NOTE = 'DELETE_NOTE';
export const CHANGE_TEMP_NOTE_TITLE = 'CHANGE_TEMP_NOTE_TITLE';
export const CHANGE_TEMP_NOTE_TEXT = 'CHANGE_TEMP_NOTE_TEXT';
export const CREATE_NOTE = 'CREATE_NOTE';
export const CHANGE_FILTER = 'CHANGE_FILTER';

import {
	createNewNoteChanger,
	changeTempNoteTitleChanger,
	changeTempNoteTextChanger,
	changeNoteTitleChanger,
	changeNoteTextChanger,
	deleteNoteChanger,
	changeFilterChanger
} from './notes';

export default (state, action, obj) => {
	switch(action) {
		case CHANGE_TEMP_NOTE_TITLE: return changeTempNoteTitleChanger(state, obj);
		case CHANGE_TEMP_NOTE_TEXT: return changeTempNoteTextChanger(state, obj);
		case CREATE_NOTE: return createNewNoteChanger(state, obj);
		case CHANGE_NOTE_TITLE: return changeNoteTitleChanger(state, obj);
		case CHANGE_NOTE_TEXT: return changeNoteTextChanger(state, obj);
		case DELETE_NOTE: return deleteNoteChanger(state, obj);
		case CHANGE_FILTER: return changeFilterChanger(state, obj);
		default: return state;
	}
}